import cx from 'classnames';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = ({ text, onClick }) => {

    const combinedStyle = cx(
        styles.container,
        "shadow"
    );

    return (
        <div className={combinedStyle}
            onClick={onClick}>
            {text}
        </div>
    )
}

export default Button;

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}