import Title from '../../Title';
import styles from './AllContainers.module.css';

export const SearchContainer = ({ children }) => {
    return (
        <div className={styles.search_container}>
            {children}
        </div>
    )
};

export const ResultContainer = ({ children, searchValue }) => {
    return (
        <div className={styles.result_container}>
            <Title
                containerStyle={styles.result_container_title}
                textStyle={styles.result_text_title}
                text={`No results for ”${searchValue}”`} />
            {children}
        </div>
    )
};