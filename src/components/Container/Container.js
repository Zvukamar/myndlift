import { useState } from 'react';
import cx from 'classnames';

import Button from '../Button';
import OInput from '../OInput';
import Title from '../Title';
import styles from './Container.module.css';
import { SearchContainer, ResultContainer } from './AllContainers/AllContainers';
import data from '../../utilities/dataToFilter.json';

const Container = () => {
    const [userInput, setUserInput] = useState('');
    const [searchValue, setSearchValue] = useState('');

    const onTextChange = (e) => {
        setUserInput(e.target.value);
    }

    const onSubmit = () => {
        setSearchValue(userInput);
    }

    return (
        <main className={styles.container}>
            <Title text='Nearby Myndlift Providers' />
            <SearchContainer>
                <OInput
                    placeholder='Search by city, state, or country'
                    className={cx("shadow", styles.search_input)}
                    onTextChange={onTextChange}
                    onSubmit={onSubmit}
                />
                <Button
                    text='Search'
                    onClick={onSubmit}
                />
            </SearchContainer>

            <ResultContainer searchValue={searchValue}>

            </ResultContainer>
        </main>
    )
}

export default Container;