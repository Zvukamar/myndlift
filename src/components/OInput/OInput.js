import PropTypes from 'prop-types';

const OInput = ({ placeholder, className, onTextChange, onSubmit }) => {
    const handleOnKeyPress = (e) => {
        if (e.charCode === 13) {
            onSubmit();
        }
    }
    return (
        <input
            type="text"
            placeholder={placeholder}
            className={className}
            onChange={onTextChange}
            onKeyPress={handleOnKeyPress}
            maxLength={20}
        />
    )
}

export default OInput;

OInput.propTypes = {
    placeholder: PropTypes.string,
    className: PropTypes.string,
    onTextChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}