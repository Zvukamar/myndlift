import PropTypes from 'prop-types';
import styles from './Title.module.css';
import cx from 'classnames';

const Title = ({ text, containerStyle, textStyle }) => {
    return (
        <div className={cx(styles.container, containerStyle)}>
            <div className={cx(styles.text, textStyle)}>{text}</div>
        </div>
    )
}

export default Title;

Title.propTypes = {
    text: PropTypes.string.isRequired,
    containerStyle: PropTypes.string,
    textStyle: PropTypes.string
}