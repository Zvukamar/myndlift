import PropTypes from 'prop-types';
import logoImg from './logo.png';

const Logo = ({ className }) => {
    return (
        <img
            className={className}
            alt='logo'
            src={logoImg}
        />
    );
}

export default Logo;

Logo.propTypes = {
    className: PropTypes.string
}