import Logo from '../Logo';
import styles from './Banner.module.css';

const Banner = (props) => {
    return (
        <header className={styles.container}>
            <Logo className={styles.logo}/>
        </header>
    );
}

export default Banner;