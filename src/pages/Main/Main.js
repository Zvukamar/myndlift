import styles from './Main.module.css';
import Banner from '../../components/Banner';
import Container from '../../components/Container';

const Main = () => {
  return (
    <div className={styles.container}>
        <Banner />
        <Container />
    </div>
  );
}

export default Main;
